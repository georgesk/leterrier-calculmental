# -------------------------------------------------
# Project created by QtCreator 2010-07-13T16:03:17
# -------------------------------------------------
system(ccache -V):QMAKE_CXX = ccache g++

QT += script network xml gui widgets printsupport multimedia multimediawidgets

TARGET = leterrier-calculment
TEMPLATE = app

SOURCES += main.cpp \
    interface.cpp \
    baudruche.cpp \
    boutonspolygone.cpp \
    sauvegardelog.cpp \
    editeur.cpp \
    abuledulanceurv1.cpp \
    exercicemaisonnombres.cpp \
    pixmapmaison.cpp \
    abuledulineeditv0.cpp \
    futurelib/abuleduexportpdfv1/abuleduexportpdfv1.cpp \
    calculmentgraphicsview.cpp \
    abstractexercise.cpp \
    exerciceoperation.cpp \
    abuledupixmapwidgetv1.cpp

HEADERS += interface.h \
    baudruche.h \
    boutonspolygone.h \
    sauvegardelog.h \
    editeur.h \
    abuledulanceurv1.h \
    exercicemaisonnombres.h \
    pixmapmaison.h \
    version.h \
    abuledulineeditv0.h \
    futurelib/abuleduexportpdfv1/abuleduexportpdfv1.h \
    calculmentgraphicsview.h \
    leterrierstringtransition.h \
    abstractexercise.h \
    exerciceoperation.h \
    abuledupixmapwidgetv1.h

FORMS += interface.ui \
    editeur.ui \
    abuledulanceurv1.ui

RESOURCES += \
    calculment.qrc \
    abuledupixmapwidgetv1.qrc

win32{
  OTHER_FILES += ../windows/windows.rc
  RC_FILE = ../windows/windows.rc
}

macx {
 ICON = ../macos/icones/leterrier-calculment.icns
}

INCLUDEPATH += /usr/include/terrier
LIBS += -lterrier
