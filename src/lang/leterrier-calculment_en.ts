<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>AbulEduAproposV0</name>
    <message>
        <location filename="../abuleduaproposv0.ui" line="14"/>
        <source>A Propos du logiciel</source>
        <translation>About</translation>
    </message>
    <message utf8="true">
        <location filename="../abuleduaproposv0.ui" line="27"/>
        <source>Aide intégrée</source>
        <translation>Help</translation>
    </message>
    <message utf8="true">
        <location filename="../abuleduaproposv0.ui" line="41"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Aide Calculette&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;table border=&quot;0&quot; style=&quot;-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;&quot;&gt;
&lt;tr&gt;
&lt;td style=&quot;border: none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a name=&quot;calcul_mental&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-size:xx-large; font-weight:600;&quot;&gt;C&lt;/span&gt;&lt;span style=&quot; font-size:xx-large; font-weight:600;&quot;&gt;alcul mental&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a name=&quot;presentation&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-size:x-large; font-weight:600;&quot;&gt;P&lt;/span&gt;&lt;span style=&quot; font-size:x-large; font-weight:600;&quot;&gt;résentation&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Calcul mental est un logiciel développé sur la base du logiciel Calcul réfléchi de David Lucardi. Il propose d&apos;exercer les enfants à des activités de calcul mental, notamment celles prescrites par l&apos;Éducation Nationale aux différents niveaux de l&apos;école élémentaire. Adossé à un serveur AbulÉdu (installé dans une école ou hébergé chez RyXéo), Calcul mental 1.0 enregistre l&apos;activité des utilisateurs et peut la restituer dans un objectif de validation de compétences ou d&apos;analyse d&apos;erreurs. La version 1.2 permettra pour les enseignants qui le souhaitent de renseigner directement le livret personnel de compétences des élèves qui l&apos;utiliseront. &lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a name=&quot;fonctionnalites&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-size:x-large; font-weight:600;&quot;&gt;F&lt;/span&gt;&lt;span style=&quot; font-size:x-large; font-weight:600;&quot;&gt;onctionnalités&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;data/images/leterrier-calculment-interface1.jpg&quot; /&gt; &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Les activités proposées au travers du parcours des zones de l&apos;interface sont l&apos;addition (1), la soustraction (2) ou la multiplication (3) d&apos;entiers, les tables d&apos;addition (4) et de multiplication (5), les compléments additifs (6), les multiples (7), l&apos;estimation d&apos;ordres de grandeur (8), les doubles et moitiés (en projet), la construction de « maisons des nombres » pour les entiers de 2 à 9 (en projet). Un éditeur (9) permet de modifier les nombres intervalles de nombres proposés dans certains exercices. &lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a name=&quot;utilisation&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-size:x-large; font-weight:600;&quot;&gt;U&lt;/span&gt;&lt;span style=&quot; font-size:x-large; font-weight:600;&quot;&gt;tilisation&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Les exercices correspondant aux opérations addition, soustraction et multiplication sont lancés directement par la zone cliquable correspondante, ici la multiplication. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;data/images/leterrier-calculment-multiplex.jpg&quot; /&gt; &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;L&apos;objet qui se déplace (ici le ballon de baudruche) porte le calcul à effectuer, le curseur déjà disposé dans le champ de saisie permet à l&apos;utilisateur de proposer un résultat, qui est « soumis » en cliquant sur le bouton Proposer ou en tapant sur le touche Entrée du clavier. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;data/images/leterrier-calculment-multiplres.jpg&quot; /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Ce résultat est évalué… et enregistré si un serveur AbulÉdu est accessible. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Les zones correspondant aux autres activités conduisent vers une interface secondaire qui permet de faire des choix supplémentaires, comme par exemple le nombre visé par l&apos;activité « compléments additifs », sur lesquelles des boutons (les fantômes dans l&apos;exemple ci-dessous) lanceront les exercices appropriés. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;data/images/leterrier-calculment-interface2.jpg&quot; /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a name=&quot;edition&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-size:x-large; font-weight:600;&quot;&gt;E&lt;/span&gt;&lt;span style=&quot; font-size:x-large; font-weight:600;&quot;&gt;dition&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;L&apos;éditeur de niveau permet de modifier les intervalles de valeurs des opérandes dans les opérations, mais aussi le temps disponible à chaque niveau ou le nombre d&apos;opérations proposées pour un exercice. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;data/images/leterrier-calculment-editeur.jpg&quot; /&gt; &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Aide Calculette&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;table border=&quot;0&quot; style=&quot;-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;&quot;&gt;
&lt;tr&gt;
&lt;td style=&quot;border: none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a name=&quot;calcul_mental&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-size:xx-large; font-weight:600;&quot;&gt;M&lt;/span&gt;&lt;span style=&quot; font-size:xx-large; font-weight:600;&quot;&gt;ental calculation&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a name=&quot;presentation&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-size:x-large; font-weight:600;&quot;&gt;P&lt;/span&gt;&lt;span style=&quot; font-size:x-large; font-weight:600;&quot;&gt;resentation&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Mental Calculation is a software tool based upon David Lucardi&apos;s software &apos;Calcul Reflechi&apos;. It trains children in mental calculating as stipulated by the french Ministry of education for primary schools. Working alongside an Abuledu server, &apos;Mental Calculation&apos; 1.0 records a user&apos;s activity and is able to restore work in order to explore skills and to analyse errors. &apos;Mental Calculation 1.2&apos; will allow teachers to complete handbooks itemising personal skills of users. &lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a name=&quot;fonctionnalites&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-size:x-large; font-weight:600;&quot;&gt;F&lt;/span&gt;&lt;span style=&quot; font-size:x-large; font-weight:600;&quot;&gt;unctionalities&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;data/images/leterrier-calculment-interface1.jpg&quot; /&gt; &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Proposed activities include : (1) addition, (2) subtraction, (3) multiplication of integers (4) addition tables (5) times tables (6) additional additives, (7) multiples (8) order of magnitude - (9) an editor is able to make modifications in some exercises. &lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a name=&quot;utilisation&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-size:x-large; font-weight:600;&quot;&gt;U&lt;/span&gt;&lt;span style=&quot; font-size:x-large; font-weight:600;&quot;&gt;se&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Exercises using addition, subtraction, and multiplication are launched by clicking directly on the corresponding area. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;data/images/leterrier-calculment-multiplex.jpg&quot; /&gt; &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;A moving object ( a balloon in this example) carries the calculation to be performed. Setting the cursor directly in the desired field allows the user to type an answer. This is then evaluated through clicking on &quot;suggest&quot; or typing &apos;Enter&apos;.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;data/images/leterrier-calculment-multiplres.jpg&quot; /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This result is evaluated... and recorded if an Abuledu server is available. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Areas corresponding to the activities of others lead to a secondary interface where users make their selection.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;data/images/leterrier-calculment-interface2.jpg&quot; /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a name=&quot;edition&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-size:x-large; font-weight:600;&quot;&gt;E&lt;/span&gt;&lt;span style=&quot; font-size:x-large; font-weight:600;&quot;&gt;diting&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The Level editor permits modifications on &apos;operand&apos; values. Time allowances are given for exercises. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;data/images/leterrier-calculment-editeur.jpg&quot; /&gt; &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../abuleduaproposv0.ui" line="80"/>
        <source>Ressources</source>
        <translation>Resources</translation>
    </message>
    <message utf8="true">
        <location filename="../abuleduaproposv0.ui" line="94"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Aide Calculette&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;table border=&quot;0&quot; style=&quot;-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;&quot;&gt;
&lt;tr&gt;
&lt;td style=&quot;border: none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:11pt;&quot;&gt;Le flux RSS en provenance de libre pedagosite sera automatiquement téléchargé lors du lancement de l&apos;application, ne touchez à rien de cette fenêtre.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:11pt;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:11pt;&quot;&gt;Exemple: http://libre.pedagosite.net/search/leterrier-calculment&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Aide Calculette&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;table border=&quot;0&quot; style=&quot;-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;&quot;&gt;
&lt;tr&gt;
&lt;td style=&quot;border: none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:11pt;&quot;&gt;RSS streamed from &quot;libre pedagosite&quot; is automatically downloaded when the application is launched.Do not change anything in this window.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:11pt;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:11pt;&quot;&gt;Example: http://libre.pedagosite.net/search/leterrier-calculment&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message utf8="true">
        <location filename="../abuleduaproposv0.ui" line="122"/>
        <source>Nouveautés</source>
        <translation>News</translation>
    </message>
    <message utf8="true">
        <location filename="../abuleduaproposv0.ui" line="133"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;table border=&quot;0&quot; style=&quot;-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;&quot;&gt;
&lt;tr&gt;
&lt;td style=&quot;border: none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:11pt;&quot;&gt;Le flux RSS en provenance du redmine sera automatiquement téléchargé lors du lancement de l&apos;application, ne touchez à rien de cette fenêtre.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:11pt;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:11pt;&quot;&gt;Exemple: http://redmine.ryxeo.com/projects/leterrier-calculment/news&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;table border=&quot;0&quot; style=&quot;-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;&quot;&gt;
&lt;tr&gt;
&lt;td style=&quot;border: none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:11pt;&quot;&gt;RSS streamed from Redmine is automaticaly downloaded when the application is launched. Do not change anything in this window.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:11pt;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:11pt;&quot;&gt;Example: http://redmine.ryxeo.com/projects/leterrier-calculment/news&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../abuleduaproposv0.ui" line="155"/>
        <source>Forum</source>
        <translation>Forum</translation>
    </message>
    <message utf8="true">
        <location filename="../abuleduaproposv0.ui" line="163"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;table border=&quot;0&quot; style=&quot;-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;&quot;&gt;
&lt;tr&gt;
&lt;td style=&quot;border: none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:11pt;&quot;&gt;Le flux RSS en provenance du forum abuledu sera automatiquement téléchargé lors du lancement de l&apos;application, ne touchez à rien de cette fenêtre.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:11pt;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:11pt;&quot;&gt;Exemple: http://forum.abuledu.org/rss/topic/leterrier-calculment/lang/fr&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;table border=&quot;0&quot; style=&quot;-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;&quot;&gt;
&lt;tr&gt;
&lt;td style=&quot;border: none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:11pt;&quot;&gt;RSS streamed from Abuledu Forum is automaticaly downloaded when the application is launched. Do not change anything in this window.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:11pt;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:11pt;&quot;&gt;Example: http://forum.abuledu.org/rss/topic/leterrier-calculment/lang/fr&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../abuleduaproposv0.ui" line="182"/>
        <location filename="../abuleduaproposv0.cpp" line="52"/>
        <source>Posez votre question sur le forum des utilisateurs ...</source>
        <translation>Ask your questions on user&apos;s forum...</translation>
    </message>
    <message>
        <location filename="../abuleduaproposv0.ui" line="198"/>
        <source>Contribuez</source>
        <translation>Contribute</translation>
    </message>
    <message utf8="true">
        <location filename="../abuleduaproposv0.ui" line="206"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;table border=&quot;0&quot; style=&quot;-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;&quot;&gt;
&lt;tr&gt;
&lt;td style=&quot;border: none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Vous appréciez les logiciels d&apos;AbulÉdu et vous souhaitez nous aider à en développer d&apos;autres et améliorer ceux qui existent déjà ?&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;1. Achetez du service auprès de la société RyXéo qui porte le projet AbulÉdu et embauche des développeurs de logiciels libres. &lt;a href=&quot;https://ryxeo.com/boutique/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://ryxeo.com/boutique/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;2. Faites un don à l&apos;association abuledu-fr.org pour lui permettre de prendre en charge les frais de déplacements des bénévoles et assurer sa mission. &lt;a href=&quot;http://abuledu-fr.org/Faire-un-don-a-l-association.html&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://abuledu-fr.org/Faire-un-don-a-l-association.html&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;3. Participez au forum d&apos;entraide en donnant un coup de pouce à ceux qui ne savent pas encore se servir aussi bien que vous des différents logiciels&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;4. Traduisez ce logiciel dans d&apos;autres langues, pour celà proposez votre aide sur la liste dev@abuledu.org. Allez sur le site suivant pour vous y abonner: &lt;a href=&quot;http://listes.abuledu.org/wws/info/dev&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://listes.abuledu.org/wws/info/dev&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;5. Faites nous connaître auprès de vos amis, contacts et tout autour de vous, plus nous sommes nombreux et plus nos projets avanceront vite et bien.&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;table border=&quot;0&quot; style=&quot;-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;&quot;&gt;
&lt;tr&gt;
&lt;td style=&quot;border: none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Teachers or Educators who appreciate Abuledu software and wish to help in its implemenation/improvement.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;1. Buy the services of the RyXeo company - RyXeo carries the Abeledu project and hires developers. &lt;a href=&quot;https://ryxeo.com/boutique/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://ryxeo.com/boutique/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;2. Make a donation to abuledu-fr.org association - promoting the programme, covering expenses and assisting volunteers.&lt;a href=&quot;http://abuledu-fr.org/Faire-un-don-a-l-association.html&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://abuledu-fr.org/Faire-un-don-a-l-association.html&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;3. Participate in a &apos;help&apos; forum to give a boost to people who are not familiar with this software.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;4. Offer translating assistance  by joining the  - dev@abuledu.org mailing list.&lt;a href=&quot;http://listes.abuledu.org/wws/info/dev&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://listes.abuledu.org/wws/info/dev&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;5. Make us known to friends and collegues. Progress is dependent upon publicity. .&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message utf8="true">
        <location filename="../abuleduaproposv0.ui" line="233"/>
        <source>À propos</source>
        <translation>About</translation>
    </message>
    <message utf8="true">
        <location filename="../abuleduaproposv0.ui" line="290"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;table border=&quot;0&quot; style=&quot;-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;&quot;&gt;
&lt;tr&gt;
&lt;td style=&quot;border: none;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Calcul-Mental est un logiciel libre sous licence GNU/GPL 2.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Auteur:&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;   - Philippe Cadaugade &amp;lt;philippe.cadaugade@ryxeo.com&amp;gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Interface et ressources graphiques:&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;   - Arnaud Pérat &amp;lt;arnaud.perat@ryxeo.com&amp;gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Contributeurs:&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;   - Eric Seigne &amp;lt;eric.seigne@ryxeo.com&amp;gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;   - Jean-Louis Frucot &amp;lt;frucot.jeanlouis@free.fr&amp;gt; (boite à propos)&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;table border=&quot;0&quot; style=&quot;-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;&quot;&gt;
&lt;tr&gt;
&lt;td style=&quot;border: none;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Mental calculation is a free software under GNU/GPL2 licence&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Author :&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;   - Philippe Cadaugade &amp;lt;philippe.cadaugade@ryxeo.com&amp;gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Interface and graphic ressources :&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;   - Arnaud Pérat &amp;lt;arnaud.perat@ryxeo.com&amp;gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Contributors:&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;   - Eric Seigne &amp;lt;eric.seigne@ryxeo.com&amp;gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;   - Jean-Louis Frucot &amp;lt;frucot.jeanlouis@free.fr&amp;gt; (boite à propos)&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../abuleduaproposv0.ui" line="353"/>
        <source>Fermer</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="../abuleduaproposv0.cpp" line="45"/>
        <source>A propos de</source>
        <translation>About</translation>
    </message>
    <message>
        <location filename="../abuleduaproposv0.cpp" line="126"/>
        <source>&amp;Aide</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <location filename="../abuleduaproposv0.cpp" line="134"/>
        <source>Aide</source>
        <translation>Help</translation>
    </message>
    <message utf8="true">
        <location filename="../abuleduaproposv0.cpp" line="144"/>
        <source>Aide intégrée...</source>
        <translation>Software help desk...</translation>
    </message>
    <message utf8="true">
        <location filename="../abuleduaproposv0.cpp" line="151"/>
        <source>Ressources pédagogiques...</source>
        <translation>Teaching resources...</translation>
    </message>
    <message utf8="true">
        <location filename="../abuleduaproposv0.cpp" line="157"/>
        <source>Nouveautés...</source>
        <translation>News...</translation>
    </message>
    <message>
        <location filename="../abuleduaproposv0.cpp" line="163"/>
        <source>Forum des utilisateurs...</source>
        <translation>Users forum...</translation>
    </message>
    <message>
        <location filename="../abuleduaproposv0.cpp" line="169"/>
        <source>Contribuez...</source>
        <translation>Contribute...</translation>
    </message>
    <message utf8="true">
        <location filename="../abuleduaproposv0.cpp" line="175"/>
        <source>À Propos de ce logiciel...</source>
        <translation>About this software...</translation>
    </message>
    <message>
        <location filename="../abuleduaproposv0.cpp" line="213"/>
        <location filename="../abuleduaproposv0.cpp" line="245"/>
        <location filename="../abuleduaproposv0.cpp" line="454"/>
        <location filename="../abuleduaproposv0.cpp" line="533"/>
        <source>Le site n&apos;est pas accessible</source>
        <translation>Website is not avalaible</translation>
    </message>
    <message utf8="true">
        <location filename="../abuleduaproposv0.cpp" line="218"/>
        <location filename="../abuleduaproposv0.cpp" line="250"/>
        <location filename="../abuleduaproposv0.cpp" line="283"/>
        <source>Téléchargement en cours ... veuillez patienter.</source>
        <translation>Download in progress... please wait.</translation>
    </message>
    <message>
        <location filename="../abuleduaproposv0.cpp" line="278"/>
        <location filename="../abuleduaproposv0.cpp" line="392"/>
        <source>Le forum n&apos;est pas accessible</source>
        <translation>Forum in not available</translation>
    </message>
</context>
<context>
    <name>AbulEduExportPDFV1</name>
    <message>
        <source>&lt;center&gt;&lt;h5&gt;Utilisateur: %1 - %2 - %3&lt;/h5&gt;&lt;/center&gt;&lt;br /&gt;&lt;br /&gt;</source>
        <translation type="obsolete">&lt;center&gt;&lt;h5&gt;User: %1 - %2 - %3&lt;/h5&gt;&lt;/center&gt;&lt;br /&gt;&lt;br /&gt;</translation>
    </message>
    <message utf8="true">
        <source>&lt;table width=&quot;100%&quot; border=&quot;1&quot;&gt;&lt;tr&gt;&lt;th&gt;Question&lt;/th&gt;&lt;th&gt;Réponse&lt;/th&gt;&lt;th&gt;Réponse attendue&lt;/th&gt;&lt;th&gt;Évaluation&lt;/th&gt;&lt;/tr&gt;</source>
        <translation type="obsolete">&lt;table width=&quot;100%&quot; border=&quot;1&quot;&gt;&lt;tr&gt;&lt;th&gt;Question&lt;/th&gt;&lt;th&gt;Answer&lt;/th&gt;&lt;th&gt;Correct answer&lt;/th&gt;&lt;th&gt;Appraisal&lt;/th&gt;&lt;/tr&gt;</translation>
    </message>
</context>
<context>
    <name>AbuleduLanceurV1</name>
    <message>
        <location filename="../abuledulanceurv1.ui" line="17"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../abuledulanceurv1.ui" line="25"/>
        <source>Exercice</source>
        <translation>Exercise</translation>
    </message>
    <message>
        <location filename="../abuledulanceurv1.ui" line="32"/>
        <source>Nombre</source>
        <translation>Number</translation>
    </message>
    <message>
        <location filename="../abuledulanceurv1.ui" line="42"/>
        <source>Niveau</source>
        <translation>Level</translation>
    </message>
    <message>
        <location filename="../abuledulanceurv1.ui" line="85"/>
        <source>Utilisateur :</source>
        <translation>User :</translation>
    </message>
    <message>
        <source>Nom :</source>
        <translation type="obsolete">Last Name :</translation>
    </message>
    <message utf8="true">
        <source>Prénom :</source>
        <translation type="obsolete">First Name :</translation>
    </message>
    <message>
        <location filename="../abuledulanceurv1.ui" line="115"/>
        <source>Afficher le bilan en fin d&apos;exercice</source>
        <translation>Display report at the end</translation>
    </message>
    <message>
        <location filename="../abuledulanceurv1.ui" line="165"/>
        <source>&amp;Annuler</source>
        <translation>&amp;Cancel</translation>
    </message>
    <message>
        <location filename="../abuledulanceurv1.ui" line="185"/>
        <source>&amp;Lancer</source>
        <translation>&amp;Launch</translation>
    </message>
    <message>
        <location filename="../abuledulanceurv1.cpp" line="10"/>
        <source>Lanceur</source>
        <translation>Launcher</translation>
    </message>
    <message utf8="true">
        <location filename="../abuledulanceurv1.cpp" line="44"/>
        <location filename="../abuledulanceurv1.cpp" line="56"/>
        <location filename="../abuledulanceurv1.cpp" line="73"/>
        <location filename="../abuledulanceurv1.cpp" line="93"/>
        <source>Problème !!</source>
        <translation>Problem !!</translation>
    </message>
    <message utf8="true">
        <location filename="../abuledulanceurv1.cpp" line="44"/>
        <location filename="../abuledulanceurv1.cpp" line="93"/>
        <source>Fichier de configuration non trouvé</source>
        <translation>Settings file not found</translation>
    </message>
    <message utf8="true">
        <location filename="../abuledulanceurv1.cpp" line="49"/>
        <location filename="../abuledulanceurv1.cpp" line="98"/>
        <source>Conf trouvé</source>
        <translation>Settings file found</translation>
    </message>
    <message>
        <location filename="../abuledulanceurv1.cpp" line="56"/>
        <source>Le fichier de configuration ne contient pas la clef : niveaux_</source>
        <translation>Missing level key in settings file : niveaux_</translation>
    </message>
    <message>
        <location filename="../abuledulanceurv1.cpp" line="73"/>
        <source>Le fichier de configuration ne contient pas toutes les clefs : intitule_</source>
        <translation>Missing key in settings file : intitule_</translation>
    </message>
</context>
<context>
    <name>Editeur</name>
    <message>
        <location filename="../editeur.ui" line="20"/>
        <source>Editeur de niveau</source>
        <translation>Level editor</translation>
    </message>
    <message utf8="true">
        <location filename="../editeur.ui" line="120"/>
        <source>Opérande Gauche</source>
        <translation>Left operand</translation>
    </message>
    <message>
        <location filename="../editeur.ui" line="128"/>
        <location filename="../editeur.ui" line="203"/>
        <source>Minimum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../editeur.ui" line="142"/>
        <location filename="../editeur.ui" line="162"/>
        <location filename="../editeur.ui" line="217"/>
        <location filename="../editeur.ui" line="237"/>
        <source>Maximum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../editeur.ui" line="271"/>
        <source>Nombre Questions</source>
        <translation>Number Questions</translation>
    </message>
    <message utf8="true">
        <location filename="../editeur.ui" line="195"/>
        <source>Opérande Droit</source>
        <translation>Right operand</translation>
    </message>
    <message>
        <source>Nombre Ballons</source>
        <translation type="obsolete">Number of items</translation>
    </message>
    <message>
        <location filename="../editeur.ui" line="340"/>
        <source>Temps</source>
        <translation>Response time</translation>
    </message>
    <message>
        <location filename="../editeur.ui" line="372"/>
        <source>%v s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../editeur.ui" line="414"/>
        <source>&amp;Enregistrer et quitter</source>
        <translation>&amp;Save and quit</translation>
    </message>
    <message>
        <source>&amp;Quitter</source>
        <translation type="obsolete">&amp;Quit</translation>
    </message>
    <message>
        <location filename="../editeur.cpp" line="58"/>
        <source>Niveau1</source>
        <translation>Level1</translation>
    </message>
    <message>
        <location filename="../editeur.cpp" line="59"/>
        <source>Niveau2</source>
        <translation>Level2</translation>
    </message>
    <message>
        <location filename="../editeur.cpp" line="60"/>
        <source>Niveau3</source>
        <translation>Level3</translation>
    </message>
    <message>
        <location filename="../editeur.cpp" line="61"/>
        <source>Personnel</source>
        <translation>Personal</translation>
    </message>
    <message utf8="true">
        <location filename="../editeur.cpp" line="49"/>
        <source>Fichier paramètres déjà présent</source>
        <translation>Settings file already exists</translation>
    </message>
    <message>
        <location filename="../editeur.cpp" line="163"/>
        <source>Addition</source>
        <translation>Addition</translation>
    </message>
    <message>
        <location filename="../editeur.cpp" line="167"/>
        <source>Multiplication</source>
        <translation>Multiplication</translation>
    </message>
    <message>
        <location filename="../editeur.cpp" line="198"/>
        <source>OdG Additions</source>
        <translation>OoM Additions</translation>
    </message>
    <message>
        <location filename="../editeur.cpp" line="200"/>
        <source>OdG Soustractions</source>
        <translation>OoM Subtractions</translation>
    </message>
    <message>
        <location filename="../editeur.cpp" line="230"/>
        <source>OdG Multiplications</source>
        <translation>OoM Multiplications</translation>
    </message>
    <message utf8="true">
        <location filename="../editeur.cpp" line="257"/>
        <source>Compléments à 10</source>
        <translation>Additive complement to 10</translation>
    </message>
    <message utf8="true">
        <location filename="../editeur.cpp" line="261"/>
        <source>Compléments à 100</source>
        <translation>Additive complement to 100</translation>
    </message>
    <message utf8="true">
        <location filename="../editeur.cpp" line="263"/>
        <source>Compléments à 1000</source>
        <translation>Additive complement to 1000</translation>
    </message>
    <message>
        <location filename="../editeur.cpp" line="268"/>
        <source>Multiples de 5</source>
        <translation>Multiples of 5</translation>
    </message>
    <message>
        <location filename="../editeur.cpp" line="269"/>
        <source>Multiples de </source>
        <translation>Multiples of </translation>
    </message>
    <message>
        <location filename="../editeur.cpp" line="275"/>
        <source>Table d&apos;addition de </source>
        <translation>Addition table of </translation>
    </message>
    <message>
        <location filename="../editeur.cpp" line="277"/>
        <source>Table de multiplication par </source>
        <translation>Time table of </translation>
    </message>
    <message>
        <location filename="../editeur.cpp" line="282"/>
        <source>Maison des nombres</source>
        <translation>Home numbers</translation>
    </message>
    <message>
        <location filename="../editeur.cpp" line="165"/>
        <source>Soustraction</source>
        <translation>Subtraction</translation>
    </message>
    <message>
        <source>OdGrandeurAddition</source>
        <translation type="obsolete">OoMagnitudeAddition</translation>
    </message>
    <message>
        <source>OdGrandeurSoustraction</source>
        <translation type="obsolete">OoMagnitudeSubtraction</translation>
    </message>
    <message>
        <source>OdGrandeurMultiplication</source>
        <translation type="obsolete">OoMagnitudeMultiplication</translation>
    </message>
</context>
<context>
    <name>ExerciceMaisonNombres</name>
    <message>
        <location filename="../exercicemaisonnombres.cpp" line="38"/>
        <source>&amp;Suivant</source>
        <translation>&amp;Next</translation>
    </message>
</context>
<context>
    <name>ExerciceRepechage</name>
    <message utf8="true">
        <source>Réponse exacte : </source>
        <translation type="obsolete">Correct answer : </translation>
    </message>
    <message>
        <location filename="../exercicerepechage.cpp" line="29"/>
        <source>&amp;Suivant</source>
        <translation>&amp;Next</translation>
    </message>
    <message utf8="true">
        <location filename="../exercicerepechage.cpp" line="32"/>
        <source>Réponse exacte :  
</source>
        <translation>Correct answer : </translation>
    </message>
    <message>
        <location filename="../exercicerepechage.cpp" line="83"/>
        <source>GAGNE</source>
        <translation>WON</translation>
    </message>
    <message>
        <location filename="../exercicerepechage.cpp" line="90"/>
        <source>PERDU</source>
        <translation>LOST</translation>
    </message>
</context>
<context>
    <name>ExerciceRepechageMaisonNombres</name>
    <message>
        <location filename="../exercicerepechagemaisonnombres.cpp" line="27"/>
        <source>&amp;Suivant</source>
        <translation>&amp;Next</translation>
    </message>
    <message utf8="true">
        <location filename="../exercicerepechagemaisonnombres.cpp" line="30"/>
        <source>Réponse exacte : </source>
        <translation>Correct answer : </translation>
    </message>
    <message>
        <location filename="../exercicerepechagemaisonnombres.cpp" line="77"/>
        <source>GAGNE</source>
        <translation>WON</translation>
    </message>
    <message>
        <location filename="../exercicerepechagemaisonnombres.cpp" line="84"/>
        <source>PERDU</source>
        <translation>LOST</translation>
    </message>
</context>
<context>
    <name>InterfaceCompetence</name>
    <message>
        <location filename="../interfacecompetence.ui" line="17"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interfacecompetence.ui" line="49"/>
        <source>&amp;Fermer</source>
        <translation>&amp;Close</translation>
    </message>
    <message>
        <location filename="../interfacecompetence.cpp" line="51"/>
        <source>Tables de multiplication</source>
        <translation>Times tables</translation>
    </message>
    <message>
        <source>Table x</source>
        <translation type="obsolete">Time table of </translation>
    </message>
    <message>
        <location filename="../interfacecompetence.cpp" line="64"/>
        <source>Table x%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../interfacecompetence.cpp" line="75"/>
        <source>Trouve le complément à...</source>
        <translation>Find complement of...</translation>
    </message>
    <message utf8="true">
        <location filename="../interfacecompetence.cpp" line="82"/>
        <source>Compléments à 10</source>
        <translation>Additive complement to 10</translation>
    </message>
    <message utf8="true">
        <location filename="../interfacecompetence.cpp" line="89"/>
        <source>Compléments à 100</source>
        <translation>Additive complement to 100</translation>
    </message>
    <message utf8="true">
        <location filename="../interfacecompetence.cpp" line="96"/>
        <source>Compléments à 1000</source>
        <translation>Additive complement to 1000</translation>
    </message>
    <message>
        <location filename="../interfacecompetence.cpp" line="134"/>
        <location filename="../interfacecompetence.cpp" line="147"/>
        <source>Multiples de %1</source>
        <translation>Times tables of %1</translation>
    </message>
    <message>
        <location filename="../interfacecompetence.cpp" line="171"/>
        <source>Table +%1</source>
        <translation></translation>
    </message>
    <message>
        <source>Les maisons des nombres</source>
        <translation type="obsolete">Home numbers</translation>
    </message>
    <message>
        <source>La maison des nombres</source>
        <translation type="obsolete">Home numbers</translation>
    </message>
    <message>
        <location filename="../interfacecompetence.cpp" line="106"/>
        <source>La maison des nombres, niveau1</source>
        <translation>House of numbers, level 1</translation>
    </message>
    <message>
        <location filename="../interfacecompetence.cpp" line="114"/>
        <source>La maison des nombres, niveau2</source>
        <translation>House of numbers, level 2</translation>
    </message>
    <message>
        <location filename="../interfacecompetence.cpp" line="125"/>
        <source>Les multiples</source>
        <translation>Multiples</translation>
    </message>
    <message>
        <source>Multiples de </source>
        <translation type="obsolete">Multiple of </translation>
    </message>
    <message>
        <location filename="../interfacecompetence.cpp" line="160"/>
        <source>Tables d&apos;addition</source>
        <translation>Addition tables</translation>
    </message>
    <message>
        <source>Table +</source>
        <translation type="obsolete">Addition table of </translation>
    </message>
    <message>
        <location filename="../interfacecompetence.cpp" line="183"/>
        <source>Ordres de grandeur</source>
        <translation>Order of magnitude</translation>
    </message>
    <message>
        <location filename="../interfacecompetence.cpp" line="190"/>
        <source>... d&apos;additions</source>
        <translation>... with additions</translation>
    </message>
    <message>
        <location filename="../interfacecompetence.cpp" line="200"/>
        <source>... de soustractions</source>
        <translation>... with subtraction</translation>
    </message>
    <message>
        <location filename="../interfacecompetence.cpp" line="210"/>
        <source>... de multiplications</source>
        <translation>... with multiplications</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message utf8="true">
        <location filename="../main.cpp" line="34"/>
        <source>AbulÉdu LeTerrier -- Calcul-Mental</source>
        <translation>AbulEdu LeTerrier -- Mental calculation</translation>
    </message>
    <message>
        <location filename="../interface.cpp" line="230"/>
        <source>Calcul Mental</source>
        <translation>Mental calculation</translation>
    </message>
</context>
<context>
    <name>baudruche</name>
    <message>
        <location filename="../baudruche.cpp" line="216"/>
        <source>Ton score est de </source>
        <translation>Your score is </translation>
    </message>
    <message>
        <location filename="../baudruche.cpp" line="218"/>
        <source> point.</source>
        <translation> point.</translation>
    </message>
    <message>
        <location filename="../baudruche.cpp" line="219"/>
        <source> points.</source>
        <translation> points.</translation>
    </message>
    <message>
        <location filename="../baudruche.cpp" line="419"/>
        <source>TROP TARD...</source>
        <translation>TOO LATE...</translation>
    </message>
</context>
<context>
    <name>boutonsPolygone</name>
    <message utf8="true">
        <location filename="../boutonspolygone.cpp" line="183"/>
        <source>Problème !</source>
        <translation>Problem !</translation>
    </message>
    <message utf8="true">
        <location filename="../boutonspolygone.cpp" line="183"/>
        <source>Accès impossible au lanceur d&apos;activité sans identification correcte</source>
        <translation>Incorrect Id</translation>
    </message>
</context>
<context>
    <name>exercice</name>
    <message>
        <location filename="../exercice.ui" line="17"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Question</source>
        <translation type="obsolete">&amp;Question</translation>
    </message>
    <message>
        <source>&amp;Proposer</source>
        <translation type="obsolete">&amp;Suggest</translation>
    </message>
    <message>
        <location filename="../exercice.cpp" line="363"/>
        <source>&amp;Suivant</source>
        <translation>&amp;Next</translation>
    </message>
    <message utf8="true">
        <location filename="../exercice.ui" line="118"/>
        <source>Propose ta réponse</source>
        <translation>Propose your answer</translation>
    </message>
    <message utf8="true">
        <location filename="../exercice.ui" line="121"/>
        <source>&amp;Vérifier</source>
        <translation>&amp;Check</translation>
    </message>
    <message utf8="true">
        <source>Réponse exacte : </source>
        <translation type="obsolete">Correct answer : </translation>
    </message>
    <message utf8="true">
        <location filename="../exercice.ui" line="95"/>
        <source>&amp;Début</source>
        <translation>&amp;Begin</translation>
    </message>
    <message utf8="true">
        <source>Réponse exacte : 
</source>
        <translation type="obsolete">Correct answer : </translation>
    </message>
    <message utf8="true">
        <location filename="../exercice.ui" line="181"/>
        <source>Réponse exacte 
</source>
        <translation>Correct answer </translation>
    </message>
    <message>
        <location filename="../exercice.ui" line="203"/>
        <location filename="../exercice.ui" line="235"/>
        <source>.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exercice.ui" line="219"/>
        <source>sur</source>
        <translation>from</translation>
    </message>
    <message utf8="true">
        <location filename="../exercice.ui" line="284"/>
        <source>Refais le même exercice</source>
        <translation>Redo the same exercise</translation>
    </message>
    <message>
        <location filename="../exercice.ui" line="287"/>
        <source>&amp;Rejouer</source>
        <translation>&amp;Replay</translation>
    </message>
    <message>
        <location filename="../exercice.ui" line="317"/>
        <source>&amp;Aide</source>
        <translation>&amp;Help</translation>
    </message>
    <message utf8="true">
        <location filename="../exercice.ui" line="347"/>
        <source>Propose une autre réponse</source>
        <translation>Propose another answer</translation>
    </message>
    <message>
        <location filename="../exercice.ui" line="350"/>
        <source>&amp;Bonus</source>
        <translation>&amp;Bonus</translation>
    </message>
    <message>
        <source>Erreurs</source>
        <translation type="obsolete">Errors</translation>
    </message>
    <message utf8="true">
        <location filename="../exercice.ui" line="380"/>
        <source>Modifie les paramètres</source>
        <translation>Change settings</translation>
    </message>
    <message utf8="true">
        <source>2ème chance</source>
        <translation type="obsolete">2nd chance</translation>
    </message>
    <message>
        <location filename="../exercice.ui" line="383"/>
        <source>&amp;Editeur</source>
        <translation>&amp;Editor</translation>
    </message>
    <message>
        <location filename="../exercice.ui" line="413"/>
        <source>&amp;Quitter</source>
        <translation>&amp;Quit</translation>
    </message>
    <message utf8="true">
        <source>Complément additif à </source>
        <translation type="obsolete">Additional additive to </translation>
    </message>
    <message>
        <source>Multiples de </source>
        <translation type="obsolete">Multiple of </translation>
    </message>
    <message>
        <source>Table de multiplication par </source>
        <translation type="obsolete">Multiplication table of </translation>
    </message>
    <message>
        <source>Table d&apos;addition de </source>
        <translation type="obsolete">Addition table of </translation>
    </message>
    <message utf8="true">
        <source>Additions de nombres inférieurs à </source>
        <translation type="obsolete">Additions with numbers less than </translation>
    </message>
    <message>
        <source> et </source>
        <translation type="obsolete"> and </translation>
    </message>
    <message utf8="true">
        <source>Soustractions de nombres inférieurs à </source>
        <translation type="obsolete">Subtraction with numbers less than </translation>
    </message>
    <message utf8="true">
        <source>Multiplications de nombres inférieurs à </source>
        <translation type="obsolete">Multiplication with numbers less than </translation>
    </message>
    <message utf8="true">
        <location filename="../exercice.cpp" line="87"/>
        <source>Complément additif à %1</source>
        <translation>Additional additive to %1</translation>
    </message>
    <message>
        <location filename="../exercice.cpp" line="94"/>
        <source>Multiples de %1</source>
        <translation>Multiples of %1</translation>
    </message>
    <message>
        <location filename="../exercice.cpp" line="102"/>
        <source>Table de multiplication par %1</source>
        <translation>Time table of %1</translation>
    </message>
    <message>
        <location filename="../exercice.cpp" line="109"/>
        <source>Table d&apos;addition de %1</source>
        <translation>Addition table of %1</translation>
    </message>
    <message utf8="true">
        <location filename="../exercice.cpp" line="115"/>
        <source>Additions de nombres inférieurs à %1 et %2</source>
        <translation>Additions with numbers less than %1 and %2</translation>
    </message>
    <message utf8="true">
        <location filename="../exercice.cpp" line="123"/>
        <source>Soustractions de nombres inférieurs à %1 et %2</source>
        <translation>Subtraction with numbers less than %1 and %2</translation>
    </message>
    <message utf8="true">
        <location filename="../exercice.cpp" line="131"/>
        <source>Multiplications de nombres inférieurs à %1 et %2</source>
        <translation>Multiplication with numbers less than %1 and %2</translation>
    </message>
    <message>
        <location filename="../exercice.cpp" line="140"/>
        <source>Ordres de grandeur sur des </source>
        <translation>Order of magnitude with </translation>
    </message>
    <message>
        <location filename="../exercice.cpp" line="144"/>
        <source>additions</source>
        <translation>additions</translation>
    </message>
    <message>
        <location filename="../exercice.cpp" line="149"/>
        <source>soustractions</source>
        <translation>subtractions</translation>
    </message>
    <message>
        <location filename="../exercice.cpp" line="154"/>
        <source>multiplications</source>
        <translation>multiplications</translation>
    </message>
    <message>
        <location filename="../exercice.cpp" line="163"/>
        <source>La maison des nombres</source>
        <translation>Number&apos;s home</translation>
    </message>
    <message utf8="true">
        <location filename="../exercice.cpp" line="561"/>
        <source>Attention : n&apos;arrondis pas les nombres à 1 seul chiffre</source>
        <translation>Warning : don&apos;t round for this kind of numbers</translation>
    </message>
    <message>
        <source>OpÃ©ration inexistante</source>
        <translation type="obsolete">Non existing operation</translation>
    </message>
    <message utf8="true">
        <location filename="../exercice.cpp" line="394"/>
        <source>, ça n&apos;existe pas comme opération...</source>
        <translation>, it doesn&apos;t exist as operation...</translation>
    </message>
    <message utf8="true">
        <location filename="../exercice.cpp" line="394"/>
        <source>Opération inexistante</source>
        <translation>Unknown operation</translation>
    </message>
    <message>
        <location filename="../exercice.cpp" line="468"/>
        <source>GAGNE</source>
        <translation>WON</translation>
    </message>
    <message>
        <location filename="../exercice.cpp" line="475"/>
        <source>PERDU</source>
        <translation>LOST</translation>
    </message>
    <message utf8="true">
        <location filename="../exercice.cpp" line="558"/>
        <source>Tu dois trouver l&apos;ordre de grandeur du résultat du calcul proposé. 
Pour cela, tu vas arrondir les nombres en ne gardant qu&apos;un seul chiffre significatif, puis faire l&apos;opération sur les nombres arrondis. 
Exemple : 372 - 198 -&gt; 400 - 200 = 200</source>
        <translation>You have to find the order of magnitude of the proposed calculation.
So, round numbers to one significant figure. Perform the operation with the rounded numbers.
Example : 372 - 198 -&gt; 400 - 200 = 200</translation>
    </message>
</context>
<context>
    <name>interface</name>
    <message utf8="true">
        <location filename="../interface.cpp" line="54"/>
        <source>Langue chargée : </source>
        <translation>Launch language : </translation>
    </message>
    <message utf8="true">
        <location filename="../interface.cpp" line="67"/>
        <source>Fichier config NON trouvé</source>
        <translation>Settings file not found</translation>
    </message>
    <message utf8="true">
        <location filename="../interface.cpp" line="68"/>
        <source>Fichier config trouvé</source>
        <translation>Settings file found</translation>
    </message>
    <message>
        <location filename="../interface.cpp" line="96"/>
        <source>Faire des additions</source>
        <translation>Add</translation>
    </message>
    <message>
        <location filename="../interface.cpp" line="97"/>
        <source>Additions</source>
        <translation>Additions</translation>
    </message>
    <message>
        <location filename="../interface.cpp" line="105"/>
        <source>Faire des multiplications</source>
        <translation>Multiply</translation>
    </message>
    <message>
        <location filename="../interface.cpp" line="106"/>
        <source>Multiplications</source>
        <translation>Multiplications</translation>
    </message>
    <message utf8="true">
        <location filename="../interface.cpp" line="113"/>
        <source>Lancer l&apos;éditeur</source>
        <translation>Launch editor</translation>
    </message>
    <message>
        <location filename="../interface.cpp" line="121"/>
        <source>Quitter</source>
        <translation>Quit</translation>
    </message>
    <message>
        <location filename="../interface.cpp" line="129"/>
        <location filename="../interface.cpp" line="130"/>
        <source>Tables de multiplication</source>
        <translation>Multiplication tables</translation>
    </message>
    <message utf8="true">
        <location filename="../interface.cpp" line="138"/>
        <location filename="../interface.cpp" line="139"/>
        <source>Compléments additifs</source>
        <translation>Additional additive</translation>
    </message>
    <message>
        <location filename="../interface.cpp" line="155"/>
        <source>Faire des soustractions</source>
        <translation>Subtract</translation>
    </message>
    <message>
        <location filename="../interface.cpp" line="179"/>
        <source>Choisir son exercice</source>
        <translation>Choose an exercise</translation>
    </message>
    <message utf8="true">
        <location filename="../interface.cpp" line="199"/>
        <source>Problème !!</source>
        <translation>Problem !!</translation>
    </message>
    <message utf8="true">
        <location filename="../interface.cpp" line="199"/>
        <source>Fichier de configuration des langues non trouvé</source>
        <translation>Languages settings file not found</translation>
    </message>
    <message>
        <source>Complements additifs</source>
        <translation type="obsolete">Additional additive</translation>
    </message>
    <message>
        <location filename="../interface.cpp" line="147"/>
        <location filename="../interface.cpp" line="148"/>
        <source>Multiples</source>
        <translation>Multiples</translation>
    </message>
    <message>
        <location filename="../interface.cpp" line="156"/>
        <source>Soustractions</source>
        <translation>Subtractions</translation>
    </message>
    <message>
        <location filename="../interface.cpp" line="164"/>
        <location filename="../interface.cpp" line="165"/>
        <source>Tables d&apos;addition</source>
        <translation>Addition tables</translation>
    </message>
    <message>
        <location filename="../interface.cpp" line="172"/>
        <source>Ordres de grandeur</source>
        <translation>Order of magnitude</translation>
    </message>
</context>
<context>
    <name>interfaceClass</name>
    <message>
        <location filename="../interface.ui" line="14"/>
        <source>interface</source>
        <translation>interface</translation>
    </message>
    <message utf8="true">
        <location filename="../interface.ui" line="40"/>
        <source>Initialiser &amp;Paramètres</source>
        <translation>&amp;Reset Settings</translation>
    </message>
    <message>
        <location filename="../interface.ui" line="57"/>
        <source>&amp;Fichier</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="../interface.ui" line="63"/>
        <source>Exercices</source>
        <translation>Exercises</translation>
    </message>
    <message>
        <location filename="../interface.ui" line="67"/>
        <source>Tables d&apos;addition</source>
        <translation>Addition tables</translation>
    </message>
    <message>
        <location filename="../interface.ui" line="80"/>
        <source>Tables de multiplication</source>
        <translation>Multiplication tables</translation>
    </message>
    <message>
        <location filename="../interface.ui" line="93"/>
        <source>Multiples</source>
        <translation>Multiples</translation>
    </message>
    <message utf8="true">
        <location filename="../interface.ui" line="104"/>
        <source>Compléments</source>
        <translation>Complements</translation>
    </message>
    <message>
        <location filename="../interface.ui" line="113"/>
        <source>Ordres de grandeur</source>
        <translation>Orders of magnitude</translation>
    </message>
    <message>
        <location filename="../interface.ui" line="130"/>
        <source>Editeur</source>
        <translation>Editor</translation>
    </message>
    <message>
        <location filename="../interface.ui" line="136"/>
        <source>Langues</source>
        <translation>Languages</translation>
    </message>
    <message>
        <location filename="../interface.ui" line="141"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="../interface.ui" line="147"/>
        <source>&amp;Utilisateur</source>
        <translation>&amp;User</translation>
    </message>
    <message>
        <location filename="../interface.ui" line="161"/>
        <source>&amp;Quitter</source>
        <translation>&amp;Quit</translation>
    </message>
    <message>
        <location filename="../interface.ui" line="166"/>
        <source>Additions</source>
        <translation>Additions</translation>
    </message>
    <message>
        <location filename="../interface.ui" line="171"/>
        <source>Multiplications</source>
        <translation>Multiplications</translation>
    </message>
    <message utf8="true">
        <location filename="../interface.ui" line="256"/>
        <source>Afficher l&apos;éditeur</source>
        <translation>Display editor</translation>
    </message>
    <message>
        <location filename="../interface.ui" line="261"/>
        <source>A &amp;propos</source>
        <translation>A&amp;bout</translation>
    </message>
    <message>
        <location filename="../interface.ui" line="266"/>
        <source>Documentation utilisateurs</source>
        <translation>Information users</translation>
    </message>
    <message>
        <location filename="../interface.ui" line="271"/>
        <source>Soustractions</source>
        <translation>Subtractions</translation>
    </message>
    <message>
        <location filename="../interface.ui" line="276"/>
        <source>de 5</source>
        <translation>of 5</translation>
    </message>
    <message>
        <location filename="../interface.ui" line="281"/>
        <source>de 10</source>
        <translation>of 10</translation>
    </message>
    <message>
        <location filename="../interface.ui" line="286"/>
        <source>de 15</source>
        <translation>of 15</translation>
    </message>
    <message>
        <location filename="../interface.ui" line="291"/>
        <source>de 20</source>
        <translation>of 20</translation>
    </message>
    <message>
        <location filename="../interface.ui" line="296"/>
        <source>de 25</source>
        <translation>of 25</translation>
    </message>
    <message>
        <location filename="../interface.ui" line="301"/>
        <source>de 50</source>
        <translation>of 50</translation>
    </message>
    <message utf8="true">
        <location filename="../interface.ui" line="306"/>
        <source>à 10</source>
        <translation>to 10</translation>
    </message>
    <message utf8="true">
        <location filename="../interface.ui" line="311"/>
        <source>à 1000</source>
        <translation>to 1000</translation>
    </message>
    <message utf8="true">
        <location filename="../interface.ui" line="316"/>
        <source>à 100</source>
        <translation>to 100</translation>
    </message>
    <message utf8="true">
        <location filename="../interface.ui" line="321"/>
        <source>à un nombre aléatoire</source>
        <translation>to a random number</translation>
    </message>
    <message>
        <location filename="../interface.ui" line="326"/>
        <source>sur des additions</source>
        <translation>with additions</translation>
    </message>
    <message>
        <location filename="../interface.ui" line="331"/>
        <source>sur des soustractions</source>
        <translation>with subtractions</translation>
    </message>
    <message>
        <location filename="../interface.ui" line="336"/>
        <source>sur des multiplications</source>
        <translation>with multiplications</translation>
    </message>
    <message>
        <location filename="../interface.ui" line="341"/>
        <source>Maison des nombres</source>
        <translation>Home numbers</translation>
    </message>
    <message>
        <location filename="../interface.ui" line="352"/>
        <source>Verrouillage nombres</source>
        <translation>Numbers Lock</translation>
    </message>
    <message utf8="true">
        <location filename="../interface.ui" line="357"/>
        <source>&amp;Journal de mes activités</source>
        <translation>&amp;Activity logs</translation>
    </message>
    <message>
        <location filename="../interface.ui" line="362"/>
        <source>&amp;Changer d&apos;utilisateur</source>
        <translation>&amp;Swap user</translation>
    </message>
</context>
</TS>
