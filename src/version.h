#ifndef VERSION_H
#define VERSION_H

#define VER_FILEVERSION             2,0,2,0
#define VER_FILEVERSION_STR         "2.0.2\0"

#define VER_PRODUCTVERSION          2,0,2,0
#define VER_PRODUCTVERSION_STR      "2.0.2\0"

#define VER_COMPANYNAME_STR         "RyXeo SARL"
#define VER_FILEDESCRIPTION_STR     "AbulEdu LeTerrier -- Calcul Mental"
#define VER_INTERNALNAME_STR        "leterrier-calculment"
#define VER_LEGALCOPYRIGHT_STR      "Copyright (c) 2010-2012 RyXeo SARL"
#define VER_LEGALTRADEMARKS1_STR    "All Rights Reserved"
#define VER_LEGALTRADEMARKS2_STR    "License GNU/GPL v3"
#define VER_ORIGINALFILENAME_STR    "leterrier-calculment.exe"
#define VER_PRODUCTNAME_STR         "AbulEdu LeTerrier -- Calcul Mental"

#define VER_COMPANYDOMAIN_STR       "abuledu.org"

/** 20130624 : Ajout du numéro de version du module actuellement généré par l'application */
#define VER_UNITVERSION_STR         "2.0"

#endif // VERSION_H
